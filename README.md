# ssmdemo-plus

#### Description
搭建springBoot, kettle运行环境， 用于模仿kettle操作界面，实现执行转换后获取 preview Data数据和日志数据
1.  kettle转换文件可以正常执行


#### 使用方式

1.  拉取代码后，maven依赖环境下载
2.  下载pentaho-kettle的zip包：https://sourceforge.net/projects/pentaho/files/Pentaho 9.1/client-tools/
3.  解压后，将data-integration的plugins、system文件夹复制到自己需要的目录下，我放到D:/data-integration目录下
4.  设置配置参数resource/config/application-dev.yml中设置kettle执行环境需要的三个参数
5.  直接启动 KettleApplication 程序

具体图文说明访问链接：https://www.cnblogs.com/gne-hwz/p/15349080.html  
SpringBoot嵌入pentaho-kettle工具实现数据trans转换和job任务手动执行

#### 未解决问题

1. 启用kettle-lifecycle-listeners.xml和kettle-registry-extensions.xml插件后，会导致kettle环境初始化错误
    报错信息查看链接：https://www.cnblogs.com/gne-hwz/p/15347909.html

#### 参考资料

1.  kettle嵌入到Spring项目 https://github.com/zhaxiaodong9860/kettle-scheduler

