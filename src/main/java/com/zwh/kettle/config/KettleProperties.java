package com.zwh.kettle.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zwh
 * @date 2021/9/28 15:00
 **/
@Configuration
@ConfigurationProperties(prefix = "kettle", ignoreInvalidFields = true)
public class KettleProperties {

    /**
     * kettle软件工作目录
     */
    private String kettleHomeDir;
    /**
     * kettle插件目录
     */
    private String kettlePluginDir;

    /**
     * kettle用户目录,用于查找 system/karaf/文件夹
     */
    private String kettleUserDir;

    public String getKettleHomeDir() {
        return kettleHomeDir;
    }

    public void setKettleHomeDir(String kettleHomeDir) {
        this.kettleHomeDir = kettleHomeDir;
    }

    public String getKettlePluginDir() {
        return kettlePluginDir;
    }

    public void setKettlePluginDir(String kettlePluginDir) {
        this.kettlePluginDir = kettlePluginDir;
    }

    public String getKettleUserDir() {
        return kettleUserDir;
    }

    public void setKettleUserDir(String kettleUserDir) {
        this.kettleUserDir = kettleUserDir;
    }
}
