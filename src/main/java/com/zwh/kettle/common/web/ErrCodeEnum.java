package com.zwh.kettle.common.web;

/**
 * @author zwh
 * @date 2021/10/8 15:03
 **/
public enum ErrCodeEnum {
    /**
     * 错误代码以及消息
     */
    FAIL(0,"操作失败"),
    SUCCESS(1,"操作成功"),
    LOGIN(2,"登录成功"),
    LOGINOUT(3,"会话超时"),
    DATA_EXCEPTION(4,"数据异常"),
    //请求成功
    PARAMETER_CHECK_ERROR(101, "参数校验错误"),
    TOKEN_ERROR(102, "access_token效验错误"),
    TOKEN_EXPIRED_ERROR(103, "access_token已过期"),
    TOKEN_NOTFOUND_ERROR(104, "access_token不存在"),
    WHITE_CHECK_ERROR(110, "当前客户端不在白名单内"),

    ;

    private int code;
    private String msg;

    ErrCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
