package com.zwh.kettle.common.web;



/**
 * @author jzj
 * @Date 2019/2/20
 */
public class ResultInfo<T> {

    /**
     * 状态码
     */
    private int code;

    /**
     * 消息
     */
    private String msg;

    /**
     * 系统对象信息
     */
    private T data;

    /**
     * 带数据返回成功
     * @param data
     * @return
     */
    public static ResultInfo success(Object data){
        return new ResultInfo(ErrCodeEnum.SUCCESS.getCode(),ErrCodeEnum.SUCCESS.getMsg(),data);
    }


    /**
     * 自定义message返回
     * @param data
     * @param message
     * @return
     */
    public static ResultInfo success(Object data,int code,String message){
        return new ResultInfo(code,message,data);
    }

    /**
     * 直接返回成功
     * @return
     */
    public static ResultInfo success(){
        return new ResultInfo(ErrCodeEnum.SUCCESS.getCode(),ErrCodeEnum.SUCCESS.getMsg());
    }

    /**
     * 操作失败
     * @param msg
     * @return
     */
    public static ResultInfo fail(String msg){
        return new ResultInfo(ErrCodeEnum.FAIL.getCode(),msg);
    }

    /**
     * 带结果集错误
     * @param data
     * @param msg
     * @return
     */
    public static ResultInfo fail(Object data,String msg){
        return new ResultInfo(ErrCodeEnum.FAIL.getCode(),msg,data);
    }


    public ResultInfo(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultInfo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultInfo() {
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
