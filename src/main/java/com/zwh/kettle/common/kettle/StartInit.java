package com.zwh.kettle.common.kettle;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.zwh.kettle.config.KettleProperties;

/**
 * @author zhaxd
 */
@Component
public class StartInit implements InitializingBean {

    @Autowired
    KettleProperties kettleProperties;

	@Override
	public void afterPropertiesSet() throws Exception {
		//初始化环境***
//		com.zhaxd.common.kettle.environment.KettleInit.init();
        if(kettleProperties == null) {
            throw new RuntimeException("kettle配置为空:com.zwh.kettle.config.KettleProperties");
        }
        Constant constant = new Constant(kettleProperties);
		KettleInit.environmentInit();
		org.pentaho.di.core.KettleEnvironment.init();
	}

}
