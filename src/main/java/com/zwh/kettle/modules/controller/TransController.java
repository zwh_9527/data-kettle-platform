package com.zwh.kettle.modules.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zwh.kettle.common.web.ResultInfo;
import com.zwh.kettle.modules.dto.TransResultDto;
import com.zwh.kettle.modules.service.TransService;

/**
 * @author zwh
 * @date 2021/9/27 15:10
 **/
@Controller
@RequestMapping("${adminPath}/trans")
public class TransController {

    @Autowired
    private TransService transService;

    @RequestMapping(value = "/startTransformation")
    @ResponseBody
    public ResultInfo startTransformation(@RequestParam("file") MultipartFile file) throws IOException {
        String kettleTransContent = new String(file.getBytes(), StandardCharsets.UTF_8);
        TransResultDto result = transService.startTransWithDebugAndLastNode(kettleTransContent);
        if (result != null) {
            return ResultInfo.success(result);
        } else {
            return ResultInfo.fail("请求数据不合适");
        }
    }

}
