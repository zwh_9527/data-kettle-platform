package com.zwh.kettle.modules.dto;

import java.util.List;

/**
 * 转换节点预览数据
 * @author zwh
 * @date 2021/9/29 14:29
 **/
public class StepPreviewDataDto {
    private String stepName;
    private List<OutputField> fields;
    private Object[] dataArray;

    public List<OutputField> getFields() {
        return fields;
    }

    public void setFields(List<OutputField> fields) {
        this.fields = fields;
    }

    public Object[] getDataArray() {
        return dataArray;
    }

    public void setDataArray(Object[] dataArray) {
        this.dataArray = dataArray;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }
}
