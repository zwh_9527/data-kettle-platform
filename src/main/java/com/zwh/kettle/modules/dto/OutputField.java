package com.zwh.kettle.modules.dto;

/**
 * 输出字段定义
 * @author zwh
 * @date 2021/9/29 14:30
 **/
public class OutputField {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
