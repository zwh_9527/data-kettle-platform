package com.zwh.kettle.modules.dto;

import java.text.DecimalFormat;

import org.pentaho.di.trans.step.StepInterface;

/**
 * @author zwh
 * @date 2021/9/27 11:01
 **/
public class StepStatusDto {
    private String stepname;
    private int copy;
    private long linesRead;
    private long linesWritten;
    private long linesInput;
    private long linesOutput;
    private long linesUpdated;
    private long linesRejected;

    private long errors;
    private String statusDescription;
    private double seconds;
    private String speed;
    private String priority;
    private boolean stopped;
    private boolean paused;
    private long accumlatedRuntime;
    private final DecimalFormat speedDf = new DecimalFormat("#,###,###,###,##0");

    public StepStatusDto(){}
    public StepStatusDto(StepInterface baseStep){
        updateAll(baseStep);
    }

    public void updateAll(StepInterface baseStep){
        this.stepname = baseStep.getStepname();
        this.copy = baseStep.getCopy();
        this.linesRead += baseStep.getLinesRead();
        this.linesWritten += baseStep.getLinesWritten();
        this.linesInput += baseStep.getLinesInput();
        this.linesOutput += baseStep.getLinesOutput();
        this.linesUpdated += baseStep.getLinesUpdated();
        this.linesRejected += baseStep.getLinesRejected();
        this.errors += baseStep.getErrors();
        this.accumlatedRuntime += baseStep.getRuntime();
        this.statusDescription = baseStep.getStatus().getDescription();
        long in_proc = Math.max(this.linesInput, this.linesRead);
        long out_proc = Math.max(this.linesOutput + this.linesUpdated, this.linesWritten + this.linesRejected);
        float lapsed = (float)this.accumlatedRuntime / 1000.0F;
        double in_speed = 0.0D;
        double out_speed = 0.0D;
        if (lapsed != 0.0F) {
            in_speed = Math.floor((double)(10.0F * ((float)in_proc / lapsed))) / 10.0D;
            out_speed = Math.floor((double)(10.0F * ((float)out_proc / lapsed))) / 10.0D;
        }

        double speedNumber = in_speed > out_speed ? in_speed : out_speed;
        this.seconds = Math.floor((double)(lapsed * 10.0F) + 0.5D) / 10.0D;
        this.speed = lapsed == 0.0F ? "-" : " " + this.speedDf.format(speedNumber);
        this.priority = baseStep.isRunning() ? "   " + baseStep.rowsetInputSize() + "/" + baseStep.rowsetOutputSize() : "-";
        this.stopped = baseStep.isStopped();
        this.paused = baseStep.isPaused();
    }

    public String getStepname() {
        return stepname;
    }

    public void setStepname(String stepname) {
        this.stepname = stepname;
    }

    public int getCopy() {
        return copy;
    }

    public void setCopy(int copy) {
        this.copy = copy;
    }

    public long getLinesRead() {
        return linesRead;
    }

    public void setLinesRead(long linesRead) {
        this.linesRead = linesRead;
    }

    public long getLinesWritten() {
        return linesWritten;
    }

    public void setLinesWritten(long linesWritten) {
        this.linesWritten = linesWritten;
    }

    public long getLinesInput() {
        return linesInput;
    }

    public void setLinesInput(long linesInput) {
        this.linesInput = linesInput;
    }

    public long getLinesOutput() {
        return linesOutput;
    }

    public void setLinesOutput(long linesOutput) {
        this.linesOutput = linesOutput;
    }

    public long getLinesUpdated() {
        return linesUpdated;
    }

    public void setLinesUpdated(long linesUpdated) {
        this.linesUpdated = linesUpdated;
    }

    public long getLinesRejected() {
        return linesRejected;
    }

    public void setLinesRejected(long linesRejected) {
        this.linesRejected = linesRejected;
    }

    public long getErrors() {
        return errors;
    }

    public void setErrors(long errors) {
        this.errors = errors;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public double getSeconds() {
        return seconds;
    }

    public void setSeconds(double seconds) {
        this.seconds = seconds;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public long getAccumlatedRuntime() {
        return accumlatedRuntime;
    }

    public void setAccumlatedRuntime(long accumlatedRuntime) {
        this.accumlatedRuntime = accumlatedRuntime;
    }
}
