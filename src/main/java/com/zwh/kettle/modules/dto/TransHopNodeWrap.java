package com.zwh.kettle.modules.dto;

import java.util.Set;

import org.pentaho.di.trans.step.StepMeta;

/**
 * @author zwh
 * @date 2021/10/8 16:01
 **/
public class TransHopNodeWrap {
    private StepMeta node;
    private Set<String> fromNames;
    private Set<String> toNames;

    public TransHopNodeWrap(StepMeta node, Set<String> fromNames, Set<String> toNames) {
        this.node = node;
        this.fromNames = fromNames;
        this.toNames = toNames;
    }

    public StepMeta getNode() {
        return node;
    }

    public void setNode(StepMeta node) {
        this.node = node;
    }

    public Set<String> getFromNames() {
        return fromNames;
    }

    public void setFromNames(Set<String> fromNames) {
        this.fromNames = fromNames;
    }

    public Set<String> getToNames() {
        return toNames;
    }

    public void setToNames(Set<String> toNames) {
        this.toNames = toNames;
    }
}
