package com.zwh.kettle.modules.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pentaho.di.core.Result;
import org.pentaho.di.www.SlaveServerTransStatus;

/**
 * @author zwh
 * @date 2021/9/27 11:00
 **/
public class TransStatusDto {

    private String id;

    private String transName;

    private String statusDescription;

    private String errorDescription;

    private String loggingString;

    private int firstLoggingLineNr;

    private int lastLoggingLineNr;

    private Date logDate;

    private List<StepStatusDto> stepStatusList;

//    private Result result;

    private boolean paused;

    public TransStatusDto(){
        stepStatusList = new ArrayList<>();
    }
    public TransStatusDto(String transName, String id, String statusDescription) {
        this();
        this.transName = transName;
        this.id = id;
        this.statusDescription = statusDescription;
    }

    public TransStatusDto(SlaveServerTransStatus transStatus){
        copyProperties(transStatus);
    }

    public void copyProperties(SlaveServerTransStatus transStatus){
        this.id = transStatus.getId();
        this.transName = transStatus.getTransName();
        this.statusDescription = transStatus.getStatusDescription();
        this.errorDescription = transStatus.getErrorDescription();
        this.loggingString = transStatus.getLoggingString();
        this.firstLoggingLineNr = transStatus.getFirstLoggingLineNr();
        this.lastLoggingLineNr = transStatus.getLastLoggingLineNr();
        this.logDate = transStatus.getLogDate();
//        this.result = transStatus.getResult();
        this.paused = transStatus.isPaused();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getLoggingString() {
        return loggingString;
    }

    public void setLoggingString(String loggingString) {
        this.loggingString = loggingString;
    }

    public int getFirstLoggingLineNr() {
        return firstLoggingLineNr;
    }

    public void setFirstLoggingLineNr(int firstLoggingLineNr) {
        this.firstLoggingLineNr = firstLoggingLineNr;
    }

    public int getLastLoggingLineNr() {
        return lastLoggingLineNr;
    }

    public void setLastLoggingLineNr(int lastLoggingLineNr) {
        this.lastLoggingLineNr = lastLoggingLineNr;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public List<StepStatusDto> getStepStatusList() {
        return stepStatusList;
    }

    public void setStepStatusList(List<StepStatusDto> stepStatusList) {
        this.stepStatusList = stepStatusList;
    }

//    public Result getResult() {
//        return result;
//    }
//
//    public void setResult(Result result) {
//        this.result = result;
//    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }
}
