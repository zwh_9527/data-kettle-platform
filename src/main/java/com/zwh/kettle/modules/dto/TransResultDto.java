package com.zwh.kettle.modules.dto;

import java.util.List;

/**
 * @author zwh
 * @date 2021/9/29 14:27
 **/
public class TransResultDto {
    private TransStatusDto transStatus;
    private List<StepPreviewDataDto> stepPreviewDataDto;

    public TransStatusDto getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(TransStatusDto transStatus) {
        this.transStatus = transStatus;
    }

    public List<StepPreviewDataDto> getStepPreviewDataDto() {
        return stepPreviewDataDto;
    }

    public void setStepPreviewDataDto(List<StepPreviewDataDto> stepPreviewDataDto) {
        this.stepPreviewDataDto = stepPreviewDataDto;
    }
}
