package com.zwh.kettle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * @author zwh
 * @date 2021/9/27 15:03
 **/
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class})
@ConfigurationPropertiesScan
public class KettleApplication {

    public static void main(String[] args) {
        SpringApplication.run(KettleApplication.class, args);
    }

}
