@echo off
rem ======================================================================
rem windows startup script
rem ======================================================================

rem startup jar
@echo off
start java -jar -Dloader.path=.,../../target/lib -Dfile.encoding=utf-8 -server -Xms1024m -Xmx2048m ../../target/data-kettle-platform.jar
