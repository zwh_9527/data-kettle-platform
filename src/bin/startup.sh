cd ..
#项目路径
BASE_PATH=`pwd`
#项目配置路径
CONFIG_DIR=${BASE_PATH}"/config/"

# JVM Configuration
# -Xmx256m:设置JVM最大可用内存为256m,根据项目实际情况而定，建议最小和最大设置成一样。
# -Xms256m:设置JVM初始内存。此值可以设置与-Xmx相同,以避免每次垃圾回收完成后JVM重新分配内存
# -Xmn512m:设置年轻代大小为512m。整个JVM内存大小=年轻代大小 + 年老代大小 + 持久代大小。
#          持久代一般固定大小为64m,所以增大年轻代,将会减小年老代大小。此值对系统性能影响较大,Sun官方推荐配置为整个堆的3/8
# -XX:MetaspaceSize=64m:存储class的内存大小,该值越大触发Metaspace GC的时机就越晚
# -XX:MaxMetaspaceSize=320m:限制Metaspace增长的上限，防止因为某些情况导致Metaspace无限的使用本地内存，影响到其他程序
# -XX:-OmitStackTraceInFastThrow:解决重复异常不打印堆栈信息问题
# -Dloader.path: 设置lib包目录
#==========================================================================================
JAVA_OPT="-Dfile.encoding=utf-8 -server -Xms1024m -Xmx1024m -Xmn512m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=256m"
JAVA_OPT="${JAVA_OPT} -XX:-OmitStackTraceInFastThrow -Dloader.path=.,target/lib"

nohup java -jar ${JAVA_OPT} ${BASE_PATH}/target/data-kettle-platform.jar --spring.config.location=${CONFIG_DIR} > /dev/null &
