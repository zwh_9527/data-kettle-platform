package com.zwh.kettle;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.logging.KettleLogStore;
import org.pentaho.di.core.logging.LogLevel;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.RowAdapter;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMetaDataCombi;
import org.pentaho.di.www.cache.CarteStatusCache;
import org.pentaho.platform.engine.core.system.PentahoSystem;
import org.pentaho.platform.engine.core.system.StandaloneApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.zwh.kettle.common.kettle.KettleInit;
import com.zwh.kettle.modules.dto.OutputField;
import com.zwh.kettle.modules.dto.StepPreviewDataDto;
import com.zwh.kettle.modules.dto.StepStatusDto;
import com.zwh.kettle.modules.dto.TransStatusDto;

/**
 * @author zwh
 * @date 2021/9/24 16:55
 **/
public class KettleTest {

    /**
     * TODO karaf.bundle报错问题没解决，不启用kettle-lifecycle-listeners.xml和kettle-registry-extensions.xml
     * @param args
     */
    public static void main(String[] args) {
//        String filePath = "D:\\soft\\pdi-ce-9.1.0.0-324\\example.ktr"; //测试转换组件0922.ktr";
        String filePath = "D:\\soft\\pdi-ce-9.1.0.0-324\\testkettle.ktr"; //测试转换组件0922.ktr";
//        String filePath = "D:\\soft\\pdi-ce-9.1.0.0-324\\data-integration\\samples\\transformations\\JsonInput - read a file.ktr";
        // 获取到前一百行输出数据
        int previewDataRows = 100;
        // 转换结果预览数据和转换日志保存路径
        String transPreviewDataPath = "D:/testPreviewData.json";
        String testTransLogPath = "D:/testTransLog.json";
        try {
            /**
             * 当存在自定义编写插件扩展时，需要设置kettle的插件目录，让kettle能注册到自定义的插件
             */
//            System.getProperties().put("KETTLE_HOME", "D:/soft/pdi-ce-9.1.0.0-324/data-integration");
//            String endorsedStr = System.getProperties().getProperty("java.endorsed.dirs");
////            // 覆盖系统类的jar包目录设置
//            endorsedStr = endorsedStr == null ? "D:\\soft\\pdi-ce-9.1.0.0-324\\data-integration\\system\\karaf\\lib\\endorsed"
//                    : endorsedStr + ";D:\\data-integration\\system\\karaf\\lib\\endorsed";
//            System.getProperties().put("java.endorsed.dirs", endorsedStr);
////            // 插件目录设置
//            System.getProperties().put("KETTLE_PLUGIN_BASE_FOLDERS", "D:/soft/pdi-ce-9.1.0.0-324/data-integration/plugins");
//             PentahoSystem.init( new StandaloneApplicationContext( "D:\\soft\\pdi-ce-9.1.0.0-324\\data-integration", "" ) );

            // 环境初始化
            KettleInit.environmentInit();
            System.setProperty("pentaho.user.dir", "D:\\data-integration");
//            PentahoSystem.init( new StandaloneApplicationContext( "D:/data-integration", "" ) );
            KettleEnvironment.init();

//            StepPluginType.getInstance().getPluginFolders().add(
//                    new PluginFolder("D:\\soft\\pdi-ce-9.1.0.0-324\\data-integration\\plugins\\common-plugin",
//                            false,true));
//            StepPluginType.getInstance().getPluginFolders().add(
//                    new PluginFolder("D:\\soft\\pdi-ce-9.1.0.0-324\\data-integration\\plugins\\kettle-json-plugin",
//                            false,true));

            TransMeta transMeta = new TransMeta(filePath);
            transMeta.setName("testKettle");
            // 创建转换
            Trans trans = new Trans(transMeta);
            trans.setLogLevel(LogLevel.BASIC);
            trans.prepareExecution(null);
            // 添加节点行数据监听器，用于获取节点输出的前一百行数据
            Map<String, List<RowMetaAndData>> previewDataMap = new HashMap<>(2);
            for (StepMetaDataCombi step : trans.getSteps()) {
                List<RowMetaAndData> rowsData = new ArrayList<>();
                previewDataMap.put(step.stepname, rowsData);
                step.step.addRowListener( new RowAdapter() {
                    @Override
                    public void rowWrittenEvent(RowMetaInterface rowMeta, Object[] row ) throws KettleStepException {
                        if ( rowsData.size() < previewDataRows ) {
                            try {
                                rowsData.add( new RowMetaAndData( rowMeta, rowMeta.cloneRow( row ) ) );
                            } catch ( Exception e ) {
                                throw new KettleStepException( "Unable to clone row for metadata : " + rowMeta, e );
                            }
                        }
                    }
                } );
            }

            // 启动转换，等待执行完成
            trans.startThreads();
            trans.waitUntilFinished();


            // 处理结果和日志数据
            if (previewDataMap.size() > 0) {
                List<StepPreviewDataDto> dataList = new ArrayList<>();

                for (Map.Entry<String, List<RowMetaAndData>> stringListEntry : previewDataMap.entrySet()) {
                    StepPreviewDataDto stepPreviewDataDto = new StepPreviewDataDto();
                    List<OutputField> columns = new ArrayList<>();
                    for (ValueMetaInterface valueMetaInterface : stringListEntry.getValue().get(0).getRowMeta().getValueMetaList()) {
                        OutputField outputField = new OutputField();
                        outputField.setName(valueMetaInterface.getName());
                        outputField.setType(valueMetaInterface.getTypeDesc());
                        columns.add(outputField);
                    }
                    Object[] data = stringListEntry.getValue().stream().map(RowMetaAndData::getData).toArray();
                    stepPreviewDataDto.setDataArray(data);
                    stepPreviewDataDto.setFields(columns);
                    stepPreviewDataDto.setStepName(stringListEntry.getKey());
                    dataList.add(stepPreviewDataDto);
                }
                String getData = JSONObject.toJSONString(dataList);
                FileWriter fileWriter = new FileWriter(new File(transPreviewDataPath));
                fileWriter.write(getData);
                fileWriter.flush();
                fileWriter.close();
            }

            // 转换任务执行日志和各个节点状态处理保存到日志文件
            int lastLineNr = KettleLogStore.getLastBufferLineNr();
            String logData = KettleLogStore.getAppender().getBuffer(
                    trans.getLogChannel().getLogChannelId(), false, 0, lastLineNr ).toString();

            TransStatusDto transStatusDto = new TransStatusDto(trans.getName(), trans.getContainerObjectId(), trans.getStatus());
            transStatusDto.setFirstLoggingLineNr(0);
            transStatusDto.setLastLoggingLineNr(lastLineNr);
            transStatusDto.setLogDate(trans.getLogDate());
            for ( int i = 0; i < trans.nrSteps(); i++ ) {
                StepInterface baseStep = trans.getRunThread( i );
                if ( ( baseStep.isRunning() ) || baseStep.getStatus() != BaseStepData.StepExecutionStatus.STATUS_EMPTY ) {
                    StepStatusDto stepStatus = new StepStatusDto( baseStep );
                    transStatusDto.getStepStatusList().add( stepStatus );
                }
            }

            transStatusDto.setLoggingString(logData);
//            transStatusDto.setResult( trans.getResult() );
            transStatusDto.setPaused( trans.isPaused() );
            String tranStatusData = JSONObject.toJSONString(transStatusDto);
            FileWriter fileWriter = new FileWriter(new File(testTransLogPath));
            fileWriter.write(tranStatusData);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
